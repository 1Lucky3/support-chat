import React, { useEffect, useState } from 'react';
import AuthForm from './UserAuthForm/AuthForm';
import Designation from './UserAuthForm/Designation';
import TextField from './UserAuthForm/TextField';
//import Button from './UserAuthForm/Button';
import {Button} from "reactstrap";
import AuthLinks from './UserAuthForm/AuthLinks';
import { useFormik } from 'formik';
import { useDispatch, connect } from 'react-redux';
import { userRegistration } from '../redux/actions';
import Loading from './Loading';
import { Redirect } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (
    !/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])/g.test(values.password)
  ) {
    errors.password =
      'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре';
  } else if (values.password.length > 20 || values.password.length < 8) {
    errors.password =
      values.password.length > 20
        ? 'Must be 20 characters or less'
        : 'Must be 8 characters or more';
  }

  if (!values.passwordConfirm) {
    errors.passwordConfirm = 'Required';
  } else if (values.password !== values.passwordConfirm) {
    errors.passwordConfirm = 'Пароли не совпадают!';
  }

  return errors;
};

const Registration = ({ isLoading, userAuth, history, errorMessage }) => {
  const dispatch = useDispatch();
  const registration__button = {
    width: '100%',
    display: 'inline-block',
  };
  const registration__form = {
    display: 'flex',
    flexDirection: 'column',
  };
  const signIn__error = {
    color: 'red',
    marginLeft: '15px',
    fontWeight: '600',
    position: 'absolute',
    right: '10px',
  };

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      passwordConfirm: '',
    },
    validate,
    onSubmit: values => {
      dispatch(userRegistration(values));
    },
  });
  console.log('=======<Redirect to="/"/>===========>');
  errorMessage.length > 0 ? toast.error(errorMessage) : toast.success('Вы успешно зарегистрировались!');
  useEffect(() => {
    if (userAuth) {
      setTimeout(function () {
        history.push('/');
      }, 2000);
    }
  }, [userAuth]);

  return isLoading ? (
    <Loading />
  ) : userAuth ? (
    <ToastContainer position="top-center" autoClose={2000} limit={1} />
  ) : (
    <AuthForm>
      <Designation>Регистрация</Designation>
      <form onSubmit={formik.handleSubmit} style={registration__form}>
        <TextField
          id="email"
          label="Email"
          name="email"
          type="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        >
          {formik.errors.email ? (
            <span style={signIn__error}>{formik.errors.email}</span>
          ) : null}
        </TextField>
        <TextField
          id="password"
          label="Пароль"
          name="password"
          type="password"
          onChange={formik.handleChange}
          value={formik.values.password}
        >
          {formik.errors.password ? (
            <span style={signIn__error}>{formik.errors.password}</span>
          ) : null}
        </TextField>
        <TextField
          id="passwordConfirm"
          label="Подтверждение пароля"
          name="passwordConfirm"
          type="password"
          onChange={formik.handleChange}
          value={formik.values.passwordConfirm}
        >
          {formik.errors.passwordConfirm ? (
            <span style={signIn__error}>{formik.errors.passwordConfirm}</span>
          ) : null}
        </TextField>
        <Button color="primary" style={{margin: "15px 0 0"}}>
          <span style={registration__button}>Регистрация</span>
        </Button>
      </form>
      <AuthLinks>{{ frsLink: 'Войти', scnLink: 'Забыли пароль?' }}</AuthLinks>
    </AuthForm>
  );
};

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  userAuth: state.userAuth,
  errorMessage: state.errorMessage,
});

export default connect(mapStateToProps, null)(Registration);
