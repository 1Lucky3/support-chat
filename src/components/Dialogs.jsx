import React from 'react';
import {Input, Label} from "reactstrap";
import {useDispatch, connect} from "react-redux";
import {fetchDialog, toggleChat} from "../redux/actions";
import throttle from 'lodash.throttle'

const Dialogs = ({children, type, handleChange, index, dialogLoaded, uid}) => {
  const [hover, setHover] = React.useState(false);
  const dispatch = useDispatch();

  const dialog = {
    height: "90px",
    //width: "100%",
    lineHeight: "70px",
    padding: "10px 50px",
    fontSize: "18px",
    fontWeight: "600",
    color: "white",
    textAlign: "center",
    background: hover || type===index ? "#3596ff" : null,
    borderBottom: type===index ? "2px solid white" : "none",
  }
  React.useEffect(() => {
    console.log(uid)
    if( /*children === "Активные" &&*/ type === index /*&& dialogLoaded === false*/){
      // dispatch(fetchActive());
      // console.log("активные");
      dispatch(toggleChat(false))
      switch(children){
        case "Активные":
          return dispatch(fetchDialog("active", uid));
        case "Завершенные":
          return dispatch(fetchDialog("completed", uid));
        case "Сохраненные":
          return dispatch(fetchDialog("saved", uid));
      }
    }
  },[type])

  return (
    <Label>
      <div
        style={dialog}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        {children}
      </div>
      <Input
        type="radio"
        name="dialog"
        onChange={() => handleChange(index)}
        checked={ type === index ? true : false }
        style={{display: "none"}}
      />
    </Label>
  );
};

const mapStateToProps = state => ({
    dialogLoaded: state.dialogLoaded,
    uid: state.userToken,
})

export default connect(mapStateToProps, null)(Dialogs);