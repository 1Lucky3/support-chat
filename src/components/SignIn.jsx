import React /*useState*/ from 'react';
import { useFormik } from 'formik';
import TextField from './UserAuthForm/TextField';
import {Input,Button} from "reactstrap";
import { useDispatch, connect } from 'react-redux';
//import Error from "./Error";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVk, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { Route, Redirect, Link, useLocation } from 'react-router-dom';
import AuthForm from './UserAuthForm/AuthForm';
import Designation from './UserAuthForm/Designation';
//import Button from './UserAuthForm/Button';
import AuthLinks from './UserAuthForm/AuthLinks';
import {
  fetchAuthUser,
  signInWithGoogle,
  signInWithVk,
} from '../redux/actions';
import Loading from './Loading';
import firebase from 'firebase/compat';
import { getAuth, signInWithCustomToken } from 'firebase/auth';
import {toast, ToastContainer} from "react-toastify";
import VK, {Auth} from "react-vk";

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length > 20 || values.password.length < 5) {
    errors.password =
      values.password.length > 20
        ? 'Must be 20 characters or less'
        : 'Must be 5 characters or more';
  }

  return errors;
};

const SignInForm = ({ state }) => {
  const code = new URLSearchParams(useLocation().search);
  console.log('итоги авторизации через вк====>', code.get('code'));
  const { isLoading, userAuth, errorMessage } = state;
  const dispatch = useDispatch();
  //const [responseText, setResponseText] = useState("")
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validate,
    onSubmit: values => {
      dispatch(fetchAuthUser(values));
    },
  });
  const googleLogin = () => {
    dispatch(signInWithGoogle());
  };
  const VkLogin = () => {
     //dispatch(signInWithVk())
  };

  const signIn__form = {
    display: 'flex',
    flexDirection: 'column',
  };
  const signIn__error = {
    color: 'red',
    marginLeft: '15px',
    fontWeight: '600',
    position: 'absolute',
    right: '15px',
  };
  const signIn__button = {
    width: '100%',
    display: 'inline-block',
  };

  const signIn__linkWrapper = {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '10px 30px 0',
  };
  console.log('----------isLoading----------->', isLoading);
  console.log('----------userAuth----------->', userAuth);
  console.log(process.env.REACT_APP_VK_AUTH, 'dshfghjsdgfjhdsgfjsdgjjfhhdsg');
  if(errorMessage?.length > 0) toast.error(errorMessage)
  return (
    <Route>
      {isLoading ? (
        <Loading />
      ) : userAuth ? (
        <Redirect to="/" />
      ) : (
        <>
          <ToastContainer position="top-center" autoClose={5000} limit={1} />
          {/*<Error>{responseText.errorMessage ? 'Wrong login or password' : null}</Error>*/}
          <AuthForm>
            <Designation>Авторизация</Designation>
            <form onSubmit={formik.handleSubmit} style={signIn__form}>
              <TextField
                id="email"
                label="Email"
                name="email"
                type="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              >
                {formik.errors.email ? (
                  <span style={signIn__error}>{formik.errors.email}</span>
                ) : null}
              </TextField>
              <TextField
                id="password"
                label="Пароль"
                name="password"
                type="password"
                onChange={formik.handleChange}
                value={formik.values.password}
              >
                {formik.errors.password ? (
                  <span style={signIn__error}>{formik.errors.password}</span>
                ) : null}
              </TextField>
              <Button color="primary" type="submit" style={{margin: "30px 0 15px"}}>
                <span style={signIn__button}>Войти</span>
              </Button>
            </form>
            <div style={signIn__linkWrapper}>
                <FontAwesomeIcon
                  onClick={VkLogin}
                  icon={faVk}
                  className="fa-4x"
                  style={{ color: '#007aff' }}
                />
                {/*<VK apiId={7957443}>*/}
                {/*  <Auth onAuth={(res) => console.log(res)}/>*/}
                {/*</VK>*/}
              <div>
                <FontAwesomeIcon
                  onClick={googleLogin}
                  icon={faGoogle}
                  className="fa-4x"
                  style={{ color: '#007aff'}}
                />
              </div>
            </div>
            <AuthLinks>
              {{ frsLink: 'Зарегистрироваться', scnLink: 'Забыли пароль?' }}
            </AuthLinks>
          </AuthForm>
        </>
      )}
    </Route>
  );
};

// const mapDispatchToProps = {
//
//
// }
const mapStateToProps = state => ({
  state,
});
export default connect(mapStateToProps, null)(SignInForm);
