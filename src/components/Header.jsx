import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import firebase from 'firebase/compat';
import { connect, useDispatch } from 'react-redux';
import { getAuthToken, userSignOut } from '../redux/actions';

const Header = (shnyaga) => {
  const [user, setUser] = useState();
  const dispatch = useDispatch();
  firebase.auth().onAuthStateChanged(user => setUser(user));
  console.log('in header USER----', user);

  useEffect(() => {
    console.log(firebase.auth().currentUser, 'from useEffect!!!!!!!!!!!!!!!!');
    if(user){
      dispatch(getAuthToken(user.uid))
    }
  }, [user]);

  const header = {
    //height: '85px',
    background: '#eaf2fa',
    boxShadow: '0 0 50px 0 #baced7',
    textAlign: 'right',
    zIndex: "1",
  };
  const header__buttons = {
    display: 'inline-block',
    margin: "20px"
  };
  const signOut = () => {
    let out = firebase.auth().signOut();
    dispatch(userSignOut());
    console.log(out);
  };

  return (
    <header style={header}>
      <span>{user ? user.email : 'нет юзера'}</span>
      {user ? (
        <div style={header__buttons}>
          <Button outline color="primary" type="submit" onClick={signOut}>
            Выйти
          </Button>
        </div>
      ) : (
        <div style={header__buttons}>
          <Link to="/signIn">
            <Button outline color="primary" style={{marginRight: "5px"}}>Войти</Button>
          </Link>
          <Link to="/registration">
            <Button outline color="primary" >Регистрация</Button>
          </Link>
        </div>
      )}
    </header>
  );
};

const mapStateToProps = state => ({
  state,
});

export default connect(mapStateToProps, null)(Header);


//"Vxj6jCOHZVV6wMdpFV8dpVxGPSn1"
//"Vxj6jCOHZVV6wMdpFV8dpVxGPSn1"