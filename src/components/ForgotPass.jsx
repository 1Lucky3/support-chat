import React from 'react';
import TextField from './UserAuthForm/TextField';
//import Button from './UserAuthForm/Button';
import {Button} from "reactstrap";
import Designation from './UserAuthForm/Designation';
import AuthForm from './UserAuthForm/AuthForm';
import AuthLinks from './UserAuthForm/AuthLinks';
import { useFormik } from 'formik';
import { useDispatch, connect } from 'react-redux';
import { changePass } from '../redux/actions';
import Loading from './Loading';
import { toast, ToastContainer } from 'react-toastify';

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  return errors;
};

const ForgotPass = ({ isLoading, userAuth, errorMessage }) => {
  const dispatch = useDispatch();
  const signIn__error = {
    color: 'red',
    marginLeft: '15px',
    fontWeight: '600',
    position: 'absolute',
    right: '5px',
  };
  const toastStyle = {
    width: '500px',
  };
  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validate,
    onSubmit: values => {
      dispatch(changePass(values));
    },
  });
  errorMessage.length > 0 ? toast.error(errorMessage) : toast.success(
    'На ваш Email было отправлено письмо, с дальнейшими инструкциями сброса пароля!',
  );
  return isLoading ? (
    <Loading />
  ) : (
    <>
      {userAuth ? (
        <ToastContainer
          position="top-center"
          autoClose={null}
          limit={1}
          style={toastStyle}
        />
      ) : null}
      <AuthForm>
        <Designation>Забыли пароль</Designation>
        <form onSubmit={formik.handleSubmit}>
          <TextField
            id="email"
            label="Email"
            name="email"
            onChange={formik.handleChange}
            values={formik.values.email}
          >
            {' '}
            {formik.errors.email ? (
              <span style={signIn__error}>{formik.errors.email}</span>
            ) : null}{' '}
          </TextField>
          <Button color="primary" style={{width: "100%", margin: "15px 0 0"}}>Отправить ссылку для восстановления</Button>
        </form>
        <AuthLinks>{{ frsLink: 'Войти', scnLink: 'Регистрация' }}</AuthLinks>
      </AuthForm>
    </>
  );
};

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  userAuth: state.userAuth,
  errorMessage: state.errorMessage,
});

export default connect(mapStateToProps, null)(ForgotPass);
