import React from 'react';
import Moment from "react-moment";
import {useDispatch, connect} from "react-redux";
import {Button} from "reactstrap";
import 'moment/locale/ru';
import {openChat, saveDialog, toggleChat} from "../redux/actions";
import {Link} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import StarRating from "./UI/StarRating";


const dialogList__card = {
  height: "110px",
  display: "flex",
  border: "2px solid #3596ff",
  margin: "10px 0",
  borderRadius: "12px",
  boxShadow: "1px 0 5px black",
  background: "white",
  color: "#3596ff",
  fontWeight: "600",
  fontSize: "18px",
}
const dialogList__button_wrap = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  margin: "15px 5px",
  marginLeft: "auto",
  // alignItems: "center",
}
const dialogList__user = {
  display: "flex",
  flexDirection: "column",
}
const dialogList__pict = {
  height: "60px",
  width: "60px",
  borderRadius: "50%",
  margin: "10px 15px",
}
const dialogList__name = {
  height: "25px",
  fontSize: "12px",
  textAlign: "center"
}
const dialogList__message = {
  overflow: "hidden",
  textOverflow: "ellipsis",
  margin: "15px 5px",
  whiteSpace: "nowrap",
  maxHeight: "55px",
  lineHeight: "47px"
}
const dialogList__last_mess = {
  width: "100%",
  textAlign: "center"
}
const dialogList__button = {
  height: "30px",
  lineHeight: "10px",
  margin: "0 10px"
}


const DialogCard = ({dialog, dialogId, status, deleteSave, uid}) => {
  const [save, setSave] = React.useState(true);
  const dispatch = useDispatch();
  console.log("dialogId from card", dialogId);
  const messages = Object.values(dialog.messages);
  const handleSave = () => {
    console.log("sadasdsa")
    dispatch(saveDialog(dialog["1uh3u1h2312hu"].saved ? {dialogId, "saved": false} : {dialogId, "saved": true}));
    setSave(false)
    if(dialog["1uh3u1h2312hu"].saved) deleteSave(dialogId);
  }

  function showButton (status) {
    if((status === "active" || status === "completed") && Object.values(dialog).find(item => item.saved === false)) {
      return  <Button
                color="primary"
                style={dialogList__button}
                onClick={() => {
                  dispatch(saveDialog({dialogId, "saved": true}));
                  setSave(false)
                }}
              >
                Сохранить
              </Button>
    }
    if(status === "saved"){
      return  <Button
        color="primary"
        style={dialogList__button}
        onClick={() => {
          dispatch(saveDialog({dialogId, "saved": false}));
          deleteSave(dialogId);
        }}
      >
        Удалить
      </Button>
    }
    if(status === "new"){
      return (
          <Button
            color="primary"
            style={dialogList__button}
            onClick={() => {
              dispatch(toggleChat(true, dialogId, uid))
              //dispatch(openChat(messages))
            }}
          >
            Войти в диалог
          </Button>
      )
    }
  }

  return (
    <div style={dialogList__card}>
      <div style={dialogList__user}>
        <img
          src="https://cdn1.savepice.ru/uploads/2021/9/27/2feef6f4de8bbe9c241a67997f69d049-full.jpg"
          alt="lico clienta"
          style={dialogList__pict}
        />
        <div style={dialogList__name} >{dialog.name}</div>
      </div>
      <div style={dialogList__message}>
        {messages[messages.length-1].content}
      </div>
      <div style={dialogList__button_wrap}>
        <div style={dialogList__last_mess}>
          <Moment unix fromNow locale="ru" >
            {messages[messages.length-1].timeStamp}
          </Moment>
        </div>
        {status === "completed" && <StarRating rating={3}/>}
        {(save || status === "saved") && showButton(status)}
        {/*{ (status === "saved" || !dialog["1uh3u1h2312hu"].saved && save) &&*/}
        {/*  <Button*/}
        {/*    color="primary"*/}
        {/*    style={dialogList__button}*/}
        {/*    onClick={handleSave}*/}
        {/*  >*/}
        {/*    {dialog["1uh3u1h2312hu"].saved ? "Удалить" : "Сохранить"}*/}
        {/*  </Button>*/}

      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  status: state.status.status,
  uid: state.status.uid
})

export default connect(mapStateToProps, null)(DialogCard);