import React from 'react';
import {Input, Label} from "reactstrap";

const TextField = ({ label, onChange, value, children, ...props }) => {
  const signup__field = {
    display: 'flex',
    flexDirection: 'column',
    //width: '350px',
    position: 'relative',
    //padding: '10px 50px',
    color: '#007aff',
  };
  const signup__input = {
    borderRadius: '8px',
    margin: '15px 0',
  };

  return (
    <div style={signup__field}>
      <Label
        for={props.id}
      >
        {label}
      </Label>
      {children}
      <Input
        style={signup__input}
        onChange={onChange}
        value={value}
        {...props}
      />
    </div>
  );
};

export default TextField;
