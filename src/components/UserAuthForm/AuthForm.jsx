import React from 'react';

const AuthForm = ({ children }) => {
  const form = {
    display: 'flex',
    flexDirection: 'column',
    width: '450px',
    borderRadius: '10px',
    boxShadow: "0 0 15px rgba(21, 87, 209, 0.8)",
    margin: 'auto',
    padding: "32px",
    position: 'relative',
    top: '90px',
  };

  return <div style={form}>{children}</div>;
};

export default AuthForm;
