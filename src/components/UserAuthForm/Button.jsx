import React from 'react';

const Button = props => {
  const button = {
    borderRadius: '10px',
    border: '2px solid #3596ff',
    padding: '10px',
    margin: typeof props.margin === 'undefined' ? '15px 50px' : props.margin,
    background: '#3596ff',
    color: 'white',
    fontSize: '18px',
    fontWeight: '500',
  };
  console.log(props.signOut);
  return (
    <button style={button} onClick={props.signOut} type="submit">
      {props.children}
    </button>
  );
};

export default Button;
