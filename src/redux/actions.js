import {
  AUTHORIZATION_SUCCESS,
  GET_AUTHORIZATION,
  GET_SIGN_OUT,
  GET_REGISTRATION,
  CHANGE_PASSWORD,
  UPDATE_PASSWORD,
  SIGN_IN_WITH_GOOGLE,
  SIGN_IN_WITH_VK,
  AUTHORIZATION_FAILURE,
  GET_USER_TOKEN,
  SEARCH_DIALOG,
  GET_ACTIVE_DIALOGS,
  FETCH_ACTIVE_DIALOGS,
  SAVE_DIALOG, DELETE_SAVE, OPEN_CHAT, TOGGLE_CHAT, SEND_MESSAGE, GET_MESSAGES, GET_MESSAGES_SUCCESS,
} from './types';

export function fetchAuthUser(userValues) {
  return {
    type: GET_AUTHORIZATION,
    payload: userValues,
  };
}
export function successAuth() {
  return {
    type: AUTHORIZATION_SUCCESS,
  };
}
export function failureAuth(error) {
  return {
    type: AUTHORIZATION_FAILURE,
    payload: error,
  }
}
export function userSignOut() {
  return {
    type: GET_SIGN_OUT,
  };
}
export function userRegistration(userValues) {
  return {
    type: GET_REGISTRATION,
    payload: userValues,
  };
}
export function changePass(email) {
  return {
    type: CHANGE_PASSWORD,
    payload: email,
  };
}
export function updatePassword(password) {
  return {
    type: UPDATE_PASSWORD,
    payload: password,
  };
}
export function signInWithGoogle() {
  return {
    type: SIGN_IN_WITH_GOOGLE,
  };
}
export function signInWithVk() {
  return {
    type: SIGN_IN_WITH_VK,
  };
}
export function getAuthToken(token) {
  return {
    type: GET_USER_TOKEN,
    payload: token,
  };
}
export function searchDialog (message) {
  return {
    type: SEARCH_DIALOG,
    payload: message,
  }
}
export function fetchDialog (status, uid) {
  return {
    type: FETCH_ACTIVE_DIALOGS,
    payload: { status, uid },

  }
}
export function getActive (data, payload) {
  return {
    type: GET_ACTIVE_DIALOGS,
    data: data,
    payload: payload,
  }
}
export function saveDialog( dialogId ) {
  return {
    type: SAVE_DIALOG,
    payload: dialogId,
  }
}
export function deleteSaveDialog( dialog ) {
  return {
    type: DELETE_SAVE,
    payload: dialog
  }
}

export function openChat (messages) {
  return {
    type: OPEN_CHAT,
    payload: messages,
  }
}

export function toggleChat (toggle, dialogId, uid) {
  return {
    type: TOGGLE_CHAT,
    payload: { toggle, dialogId, uid}
  }
}

export function sendMessage(dialogId,message) {
  return {
    type: SEND_MESSAGE,
    payload: {
      dialogId: dialogId,
      message: message
    }
  }
}

export function getMessagesChat (dialogId) {
  return {
    type: GET_MESSAGES,
    payload: dialogId
  }
}
export function getMessageSuccess(data) {
  return {
    type: GET_MESSAGES_SUCCESS,
    payload: { key: data.key, messages: data.messages }
  }
}