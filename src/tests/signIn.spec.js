import React from 'react';
import SignIn from '../components/SignIn';
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });
it('should render SignIn component', () => {
  const SignInComponent = shallow(<SignIn />);
  expect(SignInComponent.find('div')).toHaveLength(1);
});
