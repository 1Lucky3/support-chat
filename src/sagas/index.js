import { userAuthSagaWatcher, userGoogleAuth, userVkAuth } from './signInSaga';
import { all, fork } from 'redux-saga/effects';
import { registrationSagaWatcher } from './RegistrationSaga';
import { changePassword, updatePassword } from './ForgorPasswordSaga';
import { getDialogs, getMessages, openChatWatcher, saveDialog, searchDialog, sendMessage } from './DialogsSaga';

export default function* rootSaga() {
  yield all([
    fork(userAuthSagaWatcher),
    fork(registrationSagaWatcher),
    fork(changePassword),
    fork(updatePassword),
    fork(userGoogleAuth),
    fork(userVkAuth),
    fork(searchDialog),
    fork(getDialogs),
    fork(saveDialog),
    fork(openChatWatcher),
    fork(sendMessage),
    //fork(getMessages)
  ]);
}
