import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_REGISTRATION } from '../redux/types';
import firebase from 'firebase/compat';
import {failureAuth, successAuth} from '../redux/actions';

export function* registrationSagaWatcher() {
  yield takeLatest(GET_REGISTRATION, registrationSagaWorker);
}

function* registrationSagaWorker({ payload }) {
  try {
    const userPayload = yield call(getRegistration, payload);
    console.log('userPayload======>', userPayload);
    yield put(successAuth());
  }
  catch (e) {
    yield put(failureAuth(e.code));
  }
}

function getRegistration({ email, password }) {
  return firebase.auth().createUserWithEmailAndPassword(email, password);
}
