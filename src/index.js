import React from 'react';
import '@fortawesome/fontawesome-svg-core/styles.css';
import ReactDOM from 'react-dom';
import App from './App';
import firebase from 'firebase/compat';
import 'firebase/firestore';
import 'firebase/auth';
import { applyMiddleware, compose, createStore } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { authReducer } from './redux/authReducer';
import rootSaga from './sagas';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

const firebaseConfig = {
  apiKey: "AIzaSyBEkiw0XA3-RwaIuCATw79TOL1fpUzZXMQ",
  authDomain: "users-data-556ed.firebaseapp.com",
  databaseURL: "https://users-data-556ed-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "users-data-556ed",
  storageBucket: "users-data-556ed.appspot.com",
  messagingSenderId: "61856004807",
  appId: "1:61856004807:web:7d9dac9e3ed275c2efdce2",
  measurementId: "G-LN7H2GQ39N"
};

firebase.initializeApp(firebaseConfig);

const saga = createSagaMiddleware();
const store = createStore(authReducer, compose(applyMiddleware(saga)));
saga.run(rootSaga); // может стоит добавить скобочки? rootSaga()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
